# davek1312/routing

Bootstraps the illuminate/routing package.

# Installation

The package is available on [Packagist](https://packagist.org/packages/davek1312/routing),
you can install it using [Composer](https://getcomposer.org/).

```bash
composer require davek1312/routing
```

# Configuration
Copy the `index.php` and `.htaccess` in `vendor\davek1312\database\davek1312` into your application's root directory.
If you already have these files in your application just add the following to your index file:
```php
<?php

$requestDispatcher = new \Davek1312\Routing\RequestDispatcher();
$requestDispatcher->dispatchRequest();
```

## Register Routes
To register your routes view the [App Package](https://packagist.org/packages/davek1312/app) documentation.

# Usage

You can view the routing documentation at [Laravel](https://laravel.com/docs/routing).