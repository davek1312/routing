<?php

namespace Davek1312\Routing;

use Davek1312\App\App;
use Illuminate\Container\Container;
use Illuminate\Events\Dispatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Router;
use Illuminate\Routing\UrlGenerator;

/**
 * Class to handle HTTP request
 *
 * @author  David Kelly <davek1312@gmail.com>
 */
class RequestDispatcher {

    /**
     * @var App
     */
    private $app;

    /**
     * RequestDispatcher constructor.
     *
     * @param App $app
     */
    public function __construct(App $app = null) {
        $this->app = $app ? $app : new App();
    }

    /**
     * Sends the response to the client's browser
     *
     * @return void
     */
    public function dispatchRequest() {
        $routes = $this->app->getRoutes();
        $container = new Container;
        $request = Request::capture();
        $container->instance('Illuminate\Http\Request', $request);
        $events = new Dispatcher($container);
        $router = new Router($events, $container);
        foreach($routes as $route) {
            require_once $route;
        }
        $redirect = new Redirector(new UrlGenerator($router->getRoutes(), $request));
        $response = $router->dispatch($request);
        $response->send();
    }

    /**
     * @return App
     */
    public function getApp() {
        return $this->app;
    }

    /**
     * @param App $app
     */
    public function setApp($app) {
        $this->app = $app;
    }
}