<?php

namespace Davek1312\Routing\Tests;

use Davek1312\App\App;
use Davek1312\Routing\RequestDispatcher;

class RequestDispatcherTest extends \PHPUnit_Framework_TestCase {

    private $app;
    private $requestDispatcher;

    public function setUp() {
        parent::setUp();
        $this->app = new App();
        $this->app->setRoutes([__DIR__.'/Mock/routes.php']);
        $this->requestDispatcher = new RequestDispatcher($this->app);
    }


    public function test__construct() {
        $this->assertEquals($this->app, $this->requestDispatcher->getApp());
        $requestDispatcher = new RequestDispatcher();
        $this->assertEquals(new App(), $requestDispatcher->getApp());
    }

    public function testDispatchRequest() {
        $this->requestDispatcher->dispatchRequest();
    }
}